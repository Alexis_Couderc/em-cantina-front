import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import VuejsDialog from 'vuejs-dialog'
import 'vuejs-dialog/dist/vuejs-dialog.min.css'
import Vuelidate from 'vuelidate'
import router from './router.js'
import appPWA from './app/app.js'

Vue.config.productionTip = false
Vue.use(VueRouter)
Vue.use(VuejsDialog)
Vue.use(Vuelidate)

new Vue({
  render: h => h(App),
  router,
  appPWA
}).$mount('#app')
