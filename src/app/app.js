
let deferred_banner;

const app = {
	init: () => {
		// === Gestion de la bannière d'installation
		window.addEventListener('beforeinstallprompt', (event) => {
			event.preventDefault();
			deferred_banner = event;
			console.log('PWA installation event fired', deferred_banner);
			return false;
		});

		// === init Servie Worker

		if ('serviceWorker' in window.navigator) {
			window.navigator.serviceWorker
				.register('./sw.js')
				.then(registration => {
					console.log('Service worker registration succeeded:', registration);
				})
				.catch(error => {
					console.error('Service worker registration failed', error);
				});
		}
	}
};
app.init();
