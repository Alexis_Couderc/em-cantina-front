// cache name
const cacheName = 'static-v1';

// fichiers statiques nécessaires pour l'application
const app_shell_files = [
	'./index.html',
	'./app/app.js'
];

self.addEventListener('install', (event) => {
	event.waitUntil(
		caches.open(cacheName).then((cache) => {
			cache.addAll(app_shell_files);
			console.log('Page index CACHED');
		})
	);
	console.log('[Service Worker] Installing Service Worker ...', event);
});

self.addEventListener('activate', (event) => {
	self.clients.claim();
	console.log('[Service Worker] Activating Service Worker ...', event);
});

self.addEventListener('fetch', (event) => {
	event.respondWith(
		caches.match(event.request).then((response) => {
			if (response) {
				console.log('file from CACHE', event.request.url);
				return response;
			} else {
				console.log('file from INTERNET', event.request.url);
				return fetch(event.request);
			}
		})
	);
});

self.addEventListener('notificationclick', (notifEvent) => {
	console.log(`SW : Notification action result : ${notifEvent.action}`);
	notifEvent.notification.close();
});
