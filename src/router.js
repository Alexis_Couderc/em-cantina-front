import VueRouter from 'vue-router'

import Home from './components/Home.vue'
import RecipeAdd from './components/RecipeAdd.vue'
import RecipeEdit from './components/RecipeEdit.vue'
import Recipe from './components/Recipe.vue'

const routes = [
   { path: '/', component: Home },
   { path: '/add', component: RecipeAdd },
   { path: '/edit/:id', component: RecipeEdit },
   { path: '/recipe/:id', component: Recipe },
];

const router = new VueRouter({
   routes
});

export default router